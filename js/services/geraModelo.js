const ARMARIO = 0;
const GAVETAS = 1;
const PRATELEIRAS = 2;

const ALTURA_GAVETAS = 15;
const ALTURA_PRATELEIRAS = 25;

var listaTodosMateriais = [];
var listaTodosMeshes = [];
var listaTodosItens = [];

var listaTodosMeshesAUsar = [];

var todosItensParaEliminar = [];
var todasMeshesParaEliminar = [];

function desenharItem(item, scene) {
  item.limparListaMesh();
  criarEstrutura(item, scene);
  let i;
  for (i = 0; i < item.itemsFilho.length; i++) {
    desenharItem(item.itemsFilho[i], scene);
  }
}

var criarEstrutura = function (item, scene) {
  let material = personalizarMaterial(item);
  listaTodosMateriais.push(material);
  switch (item.tipo) {
    case ARMARIO:
      criarArmario(item, scene, material);
      break;
    case GAVETAS:
      criarModuloGavetas(item, scene, material);
      break;
    case PRATELEIRAS:
      criarModuloPrateleiras(item, scene, material);
      break;
    default:

  }
}

// Estruturação de itens concretos

var criarArmario = function (item, scene, material) {
  criarQuadrado(item, scene, material);
  criarFundo(item, scene, material);
  criarPortas(item, scene, material);
}

var criarModuloGavetas = function (item, scene, material) {
  criarQuadrado(item, scene, material);
  criarFundo(item, scene, material);

  let numeroEspacos = item.altura / ALTURA_GAVETAS; // gavetas terão 15 unidades de altura

  // começa a colocar prateleiras de baixo para cima
  let i;
  for (i = 0; i < numeroEspacos; i++) {
    criarGaveta(item, (-item.altura / 2 + i * ALTURA_GAVETAS) + item.posicaoY + ALTURA_GAVETAS / 2, scene, material);
  }
}

var criarModuloPrateleiras = function (item, scene, material) {
  criarQuadrado(item, scene, material);

  let numeroEspacos = item.altura / ALTURA_PRATELEIRAS; // prateleiras estarão distanciadas ~25 unidades entre si

  // começa a colocar prateleiras de baixo para cima
  let i;
  for (i = 1; i < numeroEspacos; i++) {
    criarPrateleira(item, (-item.altura / 2 + i * ALTURA_PRATELEIRAS) + item.posicaoY, scene, material);
  }

}


// Criação de partes genéricas

var criarQuadrado = function (item, scene, material) {
  // desenhar Topo
  let geometryVertical = new THREE.BoxGeometry(item.largura - 2, 1, item.profundidade);
  let cuboTopo = new THREE.Mesh(geometryVertical, material);
  cuboTopo.position.set(0 + item.posicaoX, item.altura / 2 + item.posicaoY - 0.5, 0 + item.posicaoZ);
  adicionar(item, scene, cuboTopo);

  // desenhar Base
  let cuboBase = new THREE.Mesh(geometryVertical, material);
  cuboBase.position.set(0 + item.posicaoX, -item.altura / 2 + item.posicaoY + 0.5, 0 + item.posicaoZ);
  adicionar(item, scene, cuboBase);

  // desenhar Lado Direito
  let geometryHorizontal = new THREE.BoxGeometry(1, item.altura, item.profundidade);
  let cuboDireito = new THREE.Mesh(geometryHorizontal, material);
  cuboDireito.position.set(item.largura / 2 + item.posicaoX - 0.5, 0 + item.posicaoY, 0 + item.posicaoZ);
  adicionar(item, scene, cuboDireito);

  // desenhar Lado Esquerdo
  let cuboEsquerdo = new THREE.Mesh(geometryHorizontal, material);
  cuboEsquerdo.position.set(-item.largura / 2 + item.posicaoX + 0.5, 0 + item.posicaoY, 0 + item.posicaoZ);
  adicionar(item, scene, cuboEsquerdo);
}

var criarFundo = function (item, scene, material) {
  // desenhar Topo
  let geometry = new THREE.BoxGeometry(item.largura - 2, item.altura - 2, 1);
  let cubo = new THREE.Mesh(geometry, material);
  cubo.position.set(0 + item.posicaoX, 0 + item.posicaoY, -item.profundidade / 2 + 0.5 + item.posicaoZ);
  adicionar(item, scene, cubo);
}

// partes específicas

var criarPrateleira = function (item, altura, scene, material) {
  let geometry = new THREE.BoxGeometry(item.largura - 1, 1, item.profundidade - 1); // de modo a encaixar na caixa do módulo de prateleiras
  let cubo = new THREE.Mesh(geometry, material);
  cubo.position.set(0 + item.posicaoX, altura, 0 + item.posicaoZ); // esta altura é a altura de posição da prateleira e não do item em si, aka. "a que altura a prateleira está no módulo de prateleiras"
  adicionar(item, scene, cubo);
}

var criarGaveta = function (item, altura, scene, material) {
  let geometry = new THREE.BoxGeometry(item.largura, ALTURA_GAVETAS - 0.5, 1); // a altura tem que ser a altura das gavetas - 1
  let cubo = new THREE.Mesh(geometry, material);
  cubo.position.set(0 + item.posicaoX, altura - 0.5, item.profundidade / 2 + 0.5 + item.posicaoZ); // esta altura é a altura de posição da gaveta e não do item em si, aka. "a que altura a gaveta está no módulo de gavetas"
  adicionar(item, scene, cubo);
  cubo.name = 'gavetaA';

  let baseGaveta = new THREE.BoxGeometry(item.largura - 2, 1, item.profundidade - 1);
  let cuboBaseGaveta = new THREE.Mesh(baseGaveta, material);
  cuboBaseGaveta.position.set(0 + item.posicaoX, altura - 6, 0.5 + item.posicaoZ);
  adicionar(item, scene, cuboBaseGaveta);

  let ladoGaveta = new THREE.BoxGeometry(1, ALTURA_GAVETAS - 3, item.profundidade - 1);
  let cuboEsquerdaGaveta = new THREE.Mesh(ladoGaveta, material);
  let cuboDireitaGaveta = new THREE.Mesh(ladoGaveta, material);
  cuboEsquerdaGaveta.position.set(item.posicaoX - item.largura / 2 + 1.5, altura + 0.5, 0.5 + item.posicaoZ);
  cuboDireitaGaveta.position.set(item.posicaoX + item.largura / 2 - 1.5, altura + 0.5, 0.5 + item.posicaoZ);
  adicionar(item, scene, cuboEsquerdaGaveta);
  adicionar(item, scene, cuboDireitaGaveta);

  let fundoGaveta = new THREE.BoxGeometry(item.largura - 4, ALTURA_GAVETAS - 3, 1);
  let cuboFundoGaveta = new THREE.Mesh(fundoGaveta, material);
  cuboFundoGaveta.position.set(0 + item.posicaoX, altura + 0.5, - item.profundidade / 2 + 1.5 + item.posicaoZ);
  adicionar(item, scene, cuboFundoGaveta);

  criarMacaneta(item, altura, scene);
}

var criarMacaneta = function (item, altura, scene) {
  let materialMacaneta = new THREE.MeshPhongMaterial({
    color: 0xffffff,
    specular: 0xffffff,
    shininess: 30,
    map: new THREE.TextureLoader().load('texturas/Text5.png')
  });
  let geometryMacaneta = new THREE.BoxGeometry(1, 1, 1);
  let cuboMacaneta = new THREE.Mesh(geometryMacaneta, materialMacaneta);
  cuboMacaneta.position.set(0 + item.posicaoX, altura, item.profundidade / 2 + 1 + item.posicaoZ); // esta altura é a altura de posição da prateleira e não do item em si, aka. "a que altura a prateleira está no módulo de gavetas"
  adicionar(item, scene, cuboMacaneta);
}

var criarPortas = function (item, scene, material) {
  let materialInox = new THREE.MeshPhongMaterial({
    color: 0xffffff,
    specular: 0xffffff,
    shininess: 30,
    map: new THREE.TextureLoader().load('texturas/Text5.png')
  });
  listaTodosMateriais.push(materialInox);
  let geometry = new THREE.BoxGeometry(item.largura / 2 - 0.0625, item.altura, 1); // para haver uma pequena abertura
  let cubo1 = new THREE.Mesh(geometry, material);
  let cubo2 = new THREE.Mesh(geometry, material);

  cubo1.position.set(-item.largura / 4, 0, 0);
  cubo2.position.set(item.largura / 4, 0, 0);
  listaTodosMeshes.push(cubo1);
  listaTodosMeshes.push(cubo2);

  var pivot1 = new THREE.Object3D();
  pivot1.position.set(item.largura / 2 + item.posicaoX + 0.125, 0 + item.posicaoY, item.profundidade / 2 + item.posicaoZ);
  //pivot1.rotation.y = Math.PI / 2;

  var pivot2 = new THREE.Object3D();
  pivot2.position.set(-item.largura / 2 + item.posicaoX - 0.125, 0 + item.posicaoY, item.profundidade / 2 + item.posicaoZ);
  //criarPuxadores(item, scene);

  let geometryPuxador1 = new THREE.BoxGeometry(1, item.altura / 2, 1);
  let geometryPuxador2 = new THREE.BoxGeometry(1, 1, 1);

  let offset_puxadores = item.largura / 10 + 2;

  let puxador11 = new THREE.Mesh(geometryPuxador1, materialInox);
  let puxador12 = new THREE.Mesh(geometryPuxador2, materialInox);
  let puxador13 = new THREE.Mesh(geometryPuxador2, materialInox);
  puxador11.position.set(offset_puxadores - item.largura / 2, 0, 2.1);
  puxador12.position.set(offset_puxadores - item.largura / 2, item.altura / 4 - 0.5, 1.1);
  puxador13.position.set(offset_puxadores - item.largura / 2, -item.altura / 4 + 0.5, 1.1);

  let puxador21 = new THREE.Mesh(geometryPuxador1, materialInox);
  let puxador22 = new THREE.Mesh(geometryPuxador2, materialInox);
  let puxador23 = new THREE.Mesh(geometryPuxador2, materialInox);
  puxador21.position.set(-offset_puxadores + item.largura / 2, 0, 2.1);
  puxador22.position.set(-offset_puxadores + item.largura / 2, item.altura / 4 - 0.5, 1.1);
  puxador23.position.set(-offset_puxadores + item.largura / 2, -item.altura / 4 + 0.5, 1.1);

  pivot1.add(cubo1);
  cubo1.name = 'portaA';
  pivot1.add(puxador11);
  pivot1.add(puxador12);
  pivot1.add(puxador13);
  pivot2.add(cubo2);
  cubo2.name = 'portaA';
  pivot2.add(puxador21);
  pivot2.add(puxador22);
  pivot2.add(puxador23);

  adicionar(item, scene, pivot2);
  adicionar(item, scene, pivot1);
}

/*var criarPuxadores = function (item, scene) {
  let material = new THREE.MeshPhongMaterial({
    color: 0xffffff,
    specular: 0xffffff,
    shininess: 30,
    map: new THREE.TextureLoader().load('texturas/Text5.png')
  });
  //listaTodosMateriais.push(material);
  let geometry = new THREE.BoxGeometry(1, item.altura / 2, 1);
  let cubo1 = new THREE.Mesh(geometry, material);
  let cubo2 = new THREE.Mesh(geometry, material);
  cubo1.position.set(5 + item.posicaoX, 0 + item.posicaoY, item.profundidade / 2 + 1.5 + item.posicaoZ);
  cubo2.position.set(-5 + item.posicaoX, 0 + item.posicaoY, item.profundidade / 2 + 1.5 + item.posicaoZ);
  adicionar(item, scene, cubo1);
  adicionar(item, scene, cubo2);
}*/

// Utilitários

var adicionar = function (item, scene, mesh) {
  listaTodosMeshes.push(mesh);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add(mesh);
  item.adicionarMesh(mesh);
}

function abrirPortas(item, index, onCreate) {
  if (item.tipo == 0) abrirPorta(item, index, onCreate);
  if (item.tipo == 1) abrirGavetas(item, index, onCreate);
  let i;
  for (i = 0; i < item.itemsFilho.length; i++) {
    abrirPortas(item.itemsFilho[i], index + 1, onCreate);
  }
}

function abrirGavetas(item, index, onCreate) {
  let tween1, tween2, tween3, tween4, tween5, tween6;
  let time = 500;
  if (onCreate) time = 0;
  let i = 5, count = 0; // n mexer
  while (item.meshs[i] != null) {
    tween1 = new TWEEN.Tween(item.meshs[i].position);
    tween1.easing(TWEEN.Easing.Circular.In);
    tween1.to({ z: item.profundidade / ((index * count) + 3) + item.profundidade / 2 + 0.5 + item.posicaoZ }, time);
    tween1.start();

    tween2 = new TWEEN.Tween(item.meshs[i + 1].position);
    tween2.easing(TWEEN.Easing.Circular.In);
    tween2.to({ z: item.profundidade / ((index * count) + 3) + 0.5 + item.posicaoZ }, time);
    tween2.start();

    tween3 = new TWEEN.Tween(item.meshs[i + 2].position);
    tween3.easing(TWEEN.Easing.Circular.In);
    tween3.to({ z: item.profundidade / ((index * count) + 3) + 0.5 + item.posicaoZ }, time);
    tween3.start();

    tween4 = new TWEEN.Tween(item.meshs[i + 3].position);
    tween4.easing(TWEEN.Easing.Circular.In);
    tween4.to({ z: item.profundidade / ((index * count) + 3) + 0.5 + item.posicaoZ }, time);
    tween4.start();

    tween5 = new TWEEN.Tween(item.meshs[i + 4].position);
    tween5.easing(TWEEN.Easing.Circular.In);
    tween5.to({z : item.profundidade / ((index * count)+3) - item.profundidade / 2 + 1.5 + item.posicaoZ}, time);
    tween5.start();

    tween6 = new TWEEN.Tween(item.meshs[i + 5].position);
    tween6.easing(TWEEN.Easing.Circular.In);
    tween6.to({z : item.profundidade / ((index * count)+3) + item.profundidade / 2 + 1 + item.posicaoZ}, time);
    tween6.start();

    animate();
    i += 6;
    count++;
  }
}

function abrirPorta(item, index, onCreate) {
  let x = 1000, y = 1200;
  if (onCreate) {
    x = 0;
    y = 0;
  }

  let tween1 = new TWEEN.Tween(item.meshs[item.meshs.length - 1].rotation);
  tween1.easing(TWEEN.Easing.Circular.In);
  tween1.to({ y: (Math.PI / 2) - 0.2 * index }, x); // 1000 milisegundos
  tween1.start();

  let tween2 = new TWEEN.Tween(item.meshs[item.meshs.length - 2].rotation);
  tween2.easing(TWEEN.Easing.Circular.In);
  tween2.to({ y: (-Math.PI / 2) + 0.2 * index }, y); // 1200 milisegundos
  tween2.start();

  animate();
}


function fecharPortas(item) {
  if (item.tipo == 0) fecharPorta(item);
  if (item.tipo == 1) fecharGavetas(item);
  let i;
  for (i = 0; i < item.itemsFilho.length; i++) {
    fecharPortas(item.itemsFilho[i]);
  }
}

function fecharGavetas(item) {
  let tween1, tween2, tween3, tween4, tween5, tween6;
  let i = 5; // n mexer
  let time = 500;
  while (item.meshs[i] != null) {
    tween1 = new TWEEN.Tween(item.meshs[i].position);
    tween1.easing(TWEEN.Easing.Circular.In);
    tween1.to({ z: item.profundidade / 2 + 0.5 + item.posicaoZ }, time);
    tween1.start();

    tween2 = new TWEEN.Tween(item.meshs[i + 1].position);
    tween2.easing(TWEEN.Easing.Circular.In);
    tween2.to({ z: 0.5 + item.posicaoZ }, time);
    tween2.start();

    tween3 = new TWEEN.Tween(item.meshs[i + 2].position);
    tween3.easing(TWEEN.Easing.Circular.In);
    tween3.to({ z: 0.5 + item.posicaoZ }, time);
    tween3.start();

    tween4 = new TWEEN.Tween(item.meshs[i + 3].position);
    tween4.easing(TWEEN.Easing.Circular.In);
    tween4.to({ z: 0.5 + item.posicaoZ }, time);
    tween4.start();

    tween5 = new TWEEN.Tween(item.meshs[i + 4].position);
    tween5.easing(TWEEN.Easing.Circular.In);
    tween5.to({ z: - item.profundidade / 2 + 1.5 + item.posicaoZ }, time);
    tween5.start();

    tween6 = new TWEEN.Tween(item.meshs[i + 5].position);
    tween6.easing(TWEEN.Easing.Circular.In);
    tween6.to({ z: item.profundidade / 2 + 1 + item.posicaoZ }, time);
    tween6.start();

    animate();
    i += 6;
  }
}

function fecharPorta(item) {
  let tween1 = new TWEEN.Tween(item.meshs[item.meshs.length - 1].rotation);
  tween1.easing(TWEEN.Easing.Circular.In);
  tween1.to({ y: 0 }, 1400); // 1200 milisegundos
  tween1.start();

  let tween2 = new TWEEN.Tween(item.meshs[item.meshs.length - 2].rotation);
  tween2.easing(TWEEN.Easing.Circular.In);
  tween2.to({ y: 0 }, 1200); // 1000 milisegundos
  tween2.start();
}

function colocarWired(booleano) {
  let i;
  for (i = 0; i < listaTodosMateriais.length; i++) {
    listaTodosMateriais[i].wireframe = booleano;
  }
}

function buscarItemPorMesh(mesh) {
  let i;
  let j;
  console.log("Numero atual de itens" + listaTodosItens.length);
  for (i = 0; i < listaTodosItens.length; i++) {
    let itemAtual = listaTodosItens[i];
    for (j = 0; j < itemAtual.meshs.length; j++) {
      if (itemAtual.meshs[j].children.length > 0) { // no caso do pivot das portas
        let k;
        for (k = 0; k < itemAtual.meshs[j].children.length; k++) {
          if (mesh == itemAtual.meshs[j].children[k]) return itemAtual;
        }
      }
      let meshAtual = itemAtual.meshs[j];
      if (mesh == meshAtual) return itemAtual;
    }
  }
  return null;
}

// para fazer switch do material
function personalizarMaterial(item) {
  let textura = new THREE.TextureLoader().load(item.material);

  textura.wrapS = THREE.RepeatWrapping;
  textura.wrapT = THREE.RepeatWrapping;
  textura.repeat.set(8, 4);

  let material = new THREE.MeshLambertMaterial({
    color: 0xffffff,
    specular: 0xffffff,
    shininess: 30,
    map: new THREE.TextureLoader().load(item.material)
  });

  return material;
}

function removerItem(mesh) {
  if (mesh == null) {
    alert("Selecione o item que deseja remover");
  } else {
    let itemSelecionado = buscarItemPorMesh(mesh); // buscar o item, através da mesh clicada com o rato
    if (itemSelecionado.itemPai != null) { // ver se tem pai
      let itemPai2 = itemSelecionado.itemPai; //se tiver
      let novoArrayFilhos = [];
      let i;
      for (i = 0; i < itemPai2.itemsFilho.length; i++) { //javascript não tem um remove...
        if (itemPai2.itemsFilho[i] != itemSelecionado) {
          novoArrayFilhos.push(itemPai2.itemsFilho[i]);
        }
      }
      itemPai2.itemsFilho = novoArrayFilhos; // array de filhos, já sem o item removido
    }
    buscarTodasMeshes(itemSelecionado);
    let k;
    for (k = 0; k < itemSelecionado.meshs.length; k++) {
      todasMeshesParaEliminar.push(itemSelecionado.meshs[k]);
    }
    let novaListaMeshes = [];
    let l;
    for (l = 0; l < listaTodosMeshes.length; l++) {
      if (!contemMesh(listaTodosMeshes[l], todasMeshesParaEliminar)) {
        novaListaMeshes.push(listaTodosMeshes[l]);
      }
    }
    listaTodosMeshes = novaListaMeshes;
    removerTodasMeshes(itemSelecionado);
    buscarTodosItens(itemSelecionado);
    todosItensParaEliminar.push(itemSelecionado);
    console.log("kak " + todosItensParaEliminar.length);
    let i;
    let novaListaTodosItens = [];
    for (i = 0; i < listaTodosItens.length; i++) {
      if (!contemItem(listaTodosItens[i], todosItensParaEliminar)) {
        novaListaTodosItens.push(listaTodosItens[i]);
      }
    }
    listaTodosItens = novaListaTodosItens;

    scene.remove(mesh);
    if (itemSelecionado == itemPai) {
      itemPai = new Item(0, altura, largura, profundidade, textura);
      armCriado = false;
      console.log('sai aqui');
    }
    atualizarFolders(novaListaTodosItens);
    console.log("Itens após remocao: " + listaTodosItens.length);
    todosItensParaEliminar = [];
    todasMeshesParaEliminar = [];
    meshClicada = null;
  }

}

function removerTodasMeshes(item) {
  let i;
  let j;
  for (i = 0; i < item.meshs.length; i++) {
    scene.remove(item.meshs[i]);
  }
  for (j = 0; j < item.itemsFilho.length; j++) {
    let itemFilhoAtual = item.itemsFilho[j];
    removerTodasMeshes(itemFilhoAtual);
  }
}

function colocarTudoTransparente(itemExcecao) {
  let i;
  for (i = 0; i < listaTodosItens.length; i++) {
    let itemAtual = listaTodosItens[i];
    if (itemAtual != itemExcecao) {
      let j;
      for (j = 0; j < itemAtual.meshs.length; j++) {
        let meshAtual = itemAtual.meshs[j];
        if (meshAtual.material != null) {
          meshAtual.material.transparent = true;
          meshAtual.material.opacity = 0.5;
        }

      }
    }
  }
}

function colocarTudoNormal() {
  let i;
  let j;
  for (i = 0; i < listaTodosItens.length; i++) {
    for (j = 0; j < listaTodosItens[i].meshs.length; j++) {
      if (listaTodosItens[i].meshs[j].material != null) {
        listaTodosItens[i].meshs[j].material.transparent = false;
      }
    }
  }
}

function buscarTodasMeshes(item) {
  let i;
  let j;
  for (i = 0; i < item.meshs.length; i++) {
    if (item.meshs[i].children.length > 0) {
      let k;
      for (k = 0; k < item.meshs[i].children.length; k++) {
        todasMeshesParaEliminar.push(item.meshs[i].children[k]);
      }
    }
    todasMeshesParaEliminar.push(item.meshs[i]);
  }
  for (j = 0; j < item.itemsFilho.length; j++) {
    let itemAtual = item.itemsFilho[j];
    buscarTodasMeshes(itemAtual);
  }
}

function buscarTodosItens(item) {
  let i;
  for (i = 0; i < item.itemsFilho.length; i++) {
    let itemFilhoAtual = item.itemsFilho[i];
    todosItensParaEliminar.push(itemFilhoAtual);
    buscarTodosItens(itemFilhoAtual);
  }
}

function contemItem(itemAtual, listaEliminados) {
  let i;
  for (i = 0; i < listaEliminados.length; i++) {
    if (itemAtual == listaEliminados[i]) return true;
  }
  return false;
}

function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
}

function contemMesh(meshAtual, meshsEliminadas) {
  let i;
  for (i = 0; i < meshsEliminadas.length; i++) {
    if (meshAtual == meshsEliminadas[i]) return true;
  }
  return false;
}

function verificarEvento(meshRecebida, itemRecebido) {
  let index = 0, itemDummy = itemRecebido;
  while(itemDummy.itemPai != null) {
    index++;
    itemDummy = itemDummy.itemPai;
  }
  if (meshRecebida.name == 'portaA') {
    fecharPortas(itemRecebido);
    meshRecebida.name = 'portaF';
  } else if (meshRecebida.name == 'portaF') {
    abrirPorta(itemRecebido, index, false);
    meshRecebida.name = 'portaA';
  } else if (meshRecebida.name == 'gavetaA') {
    fecharGavetas(itemRecebido);
    meshRecebida.name = 'gavetaF';
  } else if (meshRecebida.name == 'gavetaF') {
    abrirGavetas(itemRecebido, index, false);
    meshRecebida.name = 'gavetaA';
  }
}
