function ligarLuzHolofote() {
    spotLight = new THREE.SpotLight(0xffffff, 0.6, 110);
    spotLight.name = 'Spot Light';
    spotLight.position.set(-20, 13, 38);
    spotLight.castShadow = true;
    spotLight.shadow.camera.near = 1.5;
    spotLight.shadow.camera.far = 100;
    // scene.add(new THREE.CameraHelper(spotLight.shadow.camera));
}

function alterarStatusLuzHolofote(booleano) {
    if (booleano) {
        scene.add(spotLight);
    } else {
        scene.remove(spotLight);
    }
}

function alterarIntensidadeLuzHolofote(valor) {
    spotLight.intensity = valor;
}

function alterarCorHolofote(valor){
    spotLight.color.setHex(valor);
}
