function criarCamaras() {

    cameraPerspective = new THREE.PerspectiveCamera(75, aspect, 1, 2500);
    cameraPerspective.position.z = 390;
    cameraPerspective.position.y = 50;
    scene.add(cameraPerspective);

    cameraOrtho = new THREE.OrthographicCamera(- height * aspect, height * aspect, height/2, -height/2, -200, 200);
    cameraPerspective.position.z = 390;
    cameraPerspective.position.y = 50;
    scene.add(cameraOrtho);

    camera = cameraPerspective;

    controlsPerspective = new THREE.OrbitControls(cameraPerspective, renderer.domElement);
    controlsPerspective.minDistance = 80;
    controlsPerspective.maxDistance = 390;
    controlsPerspective.maxPolarAngle = (Math.PI / 2.2);    // para não deixar ver abaixo do nível do solo
    controlsPerspective.enablePan = false;
    controlsPerspective.enableDamping = false;

    controlsOrtho = new THREE.OrbitControls(cameraOrtho, renderer.domElement);
    controlsOrtho.minZoom = 20;
    controlsOrtho.maxZoom = 100;
    controlsOrtho.enablePan = false;
    controlsOrtho.enableDamping = false;

}