var OFFSET_PROFUNDIDADE = 3; // este offset existe para evitar problemas com maçanetas e puxadores

class Item {

  constructor(tipo, altura, largura, profundidade, material) {
    this.tipo = tipo;
    this.altura = altura;
    this.largura = largura;
    this.profundidade = profundidade;
    this.material = material;
    this.itemPai = null;
    this.meshs = [];
    this.itemsFilho = [];
    this.posicaoX = 0;
    this.posicaoY = altura/2; // (0,0,0) é a base do armário pai e não o seu centro
    this.posicaoZ = 0;
    this.posicaoTempX = 0;
    this.posicaoTempY = 0;
    this.posicaoTempZ = 0;
  }

  redefinirProfundidade(profundidade) {
    // verificar se nenhum dos filhos tem profundidade menor ou igual
    let i;
    for(i = 0; i < this.itemsFilho.length; i++) {
      if(profundidade-OFFSET_PROFUNDIDADE <= this.itemsFilho[i].profundidade) return false;
    }

    // verificar se cabe no pai
    if(this.itemPai != null) {
      if(profundidade >= this.itemPai.profundidade-OFFSET_PROFUNDIDADE) return false;
    }

    this.profundidade = profundidade;
    this.posicaoZ = 0;
    if(this.itemPai == null) {
      for(i = 0; i < this.itemsFilho.length; i++) {
        this.itemsFilho[i].defineNovaPosicao();
      }
    } else {
      this.defineNovaPosicao();
    }

    return true;
  }

  redefinirLargura(largura) {
    let larguraAnterior = this.largura;
    this.largura = largura;

    // verificar se este item cabe no pai com a nova dimensao
    if(this.itemPai != null) {
      if(!this.itemPai.calcularPosicaoFilhos()) {
        this.largura = larguraAnterior;
        return false;
      }
    }

    // verificar se os filhos cabem com a nova largura
	  if (!this.calcularPosicaoFilhos()) {
      this.largura = larguraAnterior;
      return false;
    }

    return true;
  }

	redefinirAltura(altura) {
    let alturaAnterior = this.altura;
    this.altura = altura;

    // verificar se este item cabe no pai com a nova dimensao
    if(this.itemPai != null) {
      if(!this.itemPai.calcularPosicaoFilhos()) {
        this.altura = alturaAnterior;
        return false;
      }
    }

    // verificar se os filhos cabem com a nova altura
    if(this.itemsFilho.length > 0) {
      if(!this.calcularPosicaoFilhos()) {
        this.altura = alturaAnterior;
        return false;
      }
    }

    // no nosso projeto, o (0,0,0) é na base do armário e não no seu centro, logo o armário pai tem que ter um offset base
    if(this.itemPai == null) {
      this.posicaoY = altura / 2;
      let i;
      for(i = 0; i < this.itemsFilho.length; i++) {
        this.itemsFilho[i].defineNovaPosicao();
      }
    } else {
      this.defineNovaPosicao();
    }

    return true;
	}

  // define os offsets das 3 dimensões, tendo em conta os valores temporários armazenados, as dimensões do próprio item e do itemPai
  defineNovaPosicao() {
    if(itemPai == null) return; // para evitar problemas com null pointers
    this.posicaoX = this.posicaoTempX + itemPai.posicaoX - itemPai.largura/2 + this.largura/2;
    this.posicaoY = this.posicaoTempY + itemPai.posicaoY - itemPai.altura/2 + this.altura/2;
    this.posicaoZ = this.posicaoTempZ + itemPai.posicaoZ - itemPai.profundidade/2 + this.profundidade/2 + 1;
    let i;
    for (i = 0; i < this.itemsFilho.length; i++) {
      this.itemsFilho[i].defineNovaPosicao();
    }
  }

  limparListaMesh() {
    this.meshs = [];
  }

  adicionarMesh(mesh) {
    this.meshs.push(mesh);
  }

  adicionarItemFilho(itemFilho) {
    if(this.tipo != 0) return false; // não adiciona se o item pai não for armário
    if(itemFilho.largura >= this.largura) return false;
	  if(itemFilho.altura >= this.altura) return false;
    if(itemFilho.profundidade >= this.profundidade-OFFSET_PROFUNDIDADE) return false;

    // reorganiza o conteúdo do armário, de modo a encaixar o novo filho
    this.itemsFilho.push(itemFilho);
    if(!this.calcularPosicaoFilhos()) {
      this.itemsFilho.splice(this.itemsFilho.length-1, 1);
      return false;
    }

    // atribui a referência deste item como itemPai
    itemFilho.itemPai = this;

    return true;
  }

  getFilhoAt(index) {
    return itemsFilho[index];
  }

  calcularPosicaoFilhos() {
    if(this.itemsFilho.length == 0) return true; // não faz nada se não houver filhos

    // constroi uma matriz de booleans a false, do tamanho do item
    let i, j, matriz = [];
    for(i = 0; i < this.altura; i++) {
      matriz[i] = [];
      for(j = 0; j < this.largura; j++) {
        if(i == 0 || j == 0 || i == this.altura -1 || j == this.largura -1) { // para evitar colisões entre meshs das paredes
          matriz[i][j] = true;
        } else {
          matriz[i][j] = false;
        }
      }
	  }

    // clona a lista de items filho para uma lista à parte, para poder ser manipulada
    let itemsFilhoClone = [];
    for(i = 0; i < this.itemsFilho.length; i++) {
      itemsFilhoClone.push(this.itemsFilho[i]);
    }

    while(itemsFilhoClone.length > 0){

      // procura o filho com maior área - TODO: arranjar maneira de verificar variações nesta ordem - se falhar à primeira, testa com outras ordens
      let maxArea = 0, indexMaxArea, currArea;
      for(i = 0; i < itemsFilhoClone.length; i++) {
        currArea = itemsFilhoClone[i].altura * itemsFilhoClone[i].largura;
        if(currArea > maxArea) {
          maxArea = currArea;
          indexMaxArea = i;
        }
      }

      // se o item não encaixa, retorna sem fazer alterações
      if(!this.encaixa(itemsFilhoClone[indexMaxArea], matriz)) return false;
      itemsFilhoClone.splice(indexMaxArea, 1);
    }

    // se todos os items filho encaixarem, define-lhes a nova posiçao
    for(i = 0; i < this.itemsFilho.length; i++) {
      this.itemsFilho[i].defineNovaPosicao();
    }

    return true;
  }

  // verifica se existe espaço suficiente para encaixar o item, tendo em conta a sua largura e altura
  espacoLivre(matriz, i, j, altura, largura) {
	  let x, y;
    for(x = i; x < i + altura; x++) {
      for(y = j; y < j + largura; y++) {
        if (matriz[x][y]) return false;
      }
	  }
    return true;
  }

  // 'encaixa' o item, ao meter a true o espaço correspondente
  encaixaAux(matriz, i, j, altura, largura) {
    let x, y;
    for (x = i; x < i + altura; x++) {
      for (y = j; y < j + largura; y++) {
        matriz[x][y] = true;
      }
    }
  }

  encaixa(item, matriz) {
    let i, j;
    for (i = 0; i < matriz.length; i++) {
      for (j = 0; j < matriz[0].length; j++) {
        if (!matriz[i][j]) { // se o lugar estiver desocupado
          if (j + item.largura < matriz[0].length && i + item.altura < matriz.length) { // se ainda couber, numericamente, no espaço restante
            if(this.espacoLivre(matriz, i, j, item.altura, item.largura)) {
              item.posicaoTempX = j; // guarda os valores no atributo temporário da classe
              item.posicaoTempY = i;
              this.encaixaAux(matriz, i, j, item.altura, item.largura);
              return true;
            }
          }
        }
      }
    }

    return false;
  }

}
