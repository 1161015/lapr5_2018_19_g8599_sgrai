const Textura1 = 'Madeira IKEA';
const Textura2 = 'Madeira Real';
const Textura3 = 'Outra Madeira';
const CAMARA_ORTOGONAL = 'Ortogonal';
const CAMARA_PERSPETIVA = 'Perspetiva';


var nomeFolder1 = 'Armário';
var nomeFolder2 = 'Modulo Gavetas';
var nomeFolder3 = 'Modulo Prateleiras';
var nomeFolder4 = 'Selecionar Armario';

var gui = new dat.GUI();

var offset_eixo_z = 100;

//atributos armario
var armCriado = false;
var speed = 1;
var altura = 60;
var largura = 60;
var profundidade = 60;

//atributos gaveta(s)
var alturaGav = 90;
var larguraGav = 60;
var profundidadeGav = 60;
var textInicialGav = { Textura: Textura1 };
var textGavFinal = textInicialGav;

//atributos prateleira(s)
var alturaPrat = 125;
var larguraPrat = 60;
var profundidadePrat = 60;
var textInicialPrat = { Textura: Textura1 };
var textPratFinal = textInicialPrat;

//item Pai criado, mas não desenhado
var textura = tipoTextura(Textura1);
var itemPai = new Item(0, altura, largura, profundidade, textura);
var itemSelecionado = itemPai;
//listaTodosItens.push(itemPai);

//array de armarios tipo 0
var arrayArmarios = [];
var arrayNomes = [];
arrayArmarios.push(itemPai);
arrayNomes.push('Armario 1');
var numArm = 2;

var armIni = { Armario: arrayNomes[0] };
var armFinal = armIni;
var armPratFinal = armIni;
var armGavFinal = armIni;

var listaTexturas = [Textura1, Textura2, Textura3];
var itens = [];
function displayGui() {

	gerarOpcoes(gui, 'Opções');

	criarFolderArmario();

	gui.open();
}


function tipoTextura(text) {
	switch (text) {
		case Textura2:
			return 'texturas/Text2.jpg';
		case Textura3:
			return 'texturas/Text4.jpg';
		default:
			return 'texturas/Text1.jpg';
	}
}

function gerarOpcoes(gui, nomePasta) {

	let pastaOpcoes = gui.addFolder(nomePasta);
	let wireframeInicial = { wire: false };
	let luzHolofoteInicial = { luzH: false };
	let intensidadeInicialLuzHolofote = { intensidade: 0.6 };
	let corInicial = { cor: 0xffffff };


	//let pastaDefinida = pastaOpcoes.add(cameraInicial, 'Camara', listaCamaras).name('Câmara');
	let wireframeDefinido = pastaOpcoes.add(wireframeInicial, 'wire').name('Wireframe');
	let luzHolofoteDefinida = pastaOpcoes.add(luzHolofoteInicial, 'luzH').name('Luz Holofote');
	let intensidadeLuzHolofote = pastaOpcoes.add(intensidadeInicialLuzHolofote, 'intensidade').min(0.1).max(1).step(0.1).name('Intensidade Luz');
	let colorLuzHolofote = pastaOpcoes.addColor(corInicial, 'cor').name('Cor da luz');

	colorLuzHolofote.onChange(function (valor) {
		alterarCorHolofote(valor);
	});

	intensidadeLuzHolofote.onChange(function (valor) {
		alterarIntensidadeLuzHolofote(valor);
	});


	luzHolofoteDefinida.onChange(function (booleano) {
		alterarStatusLuzHolofote(booleano);
	});


	wireframeDefinido.onChange(function (booleano) {
		colocarWired(booleano);
	});
	// pastaDefinida.onChange(function () {
	// 	switch (cameraInicial.Camara) {
	// 		case CAMARA_ORTOGONAL:
	// 			alert("Por implementar");
	// 			break;
	// 		//camera = cameraOrtho;
	// 		//break;
	// 		case CAMARA_PERSPETIVA:
	// 			camera = cameraPerspective;
	// 			break;
	// 		default:
	// 			break;
	// 	}
	// });

}

function limparScene(scene, itemPai) {
	let i, j;
	for (j = 0; j < itemPai.meshs.length; j++) {
		scene.remove(itemPai.meshs[j]);
	}
	//listaTodosMateriais = []; // limpar todos os materiais.

	for (i = 0; i < itemPai.itemsFilho.length; i++) {
		let filho = itemPai.itemsFilho[i];
		limparScene(scene, filho);
	}
}


function todosItens(itemPai) {
	let i;
	for (i = 0; i < itemPai.itemsFilho.length; i++) {
		let filho = itemPai.itemsFilho[i];
		todosItens(filho);
		itens.push(filho);
	}
}


function removeFoldersFolder(name1, name2, gui) {
	var folder = gui.__folders[name1].__folders[name2];
	if (!folder) {
		return;
	}
	folder.close();
	gui.__folders[name1].__ul.removeChild(folder.domElement.node);
	delete gui.__folders[name1].__folders[name2];
	gui.onResize();
}
function removeFolder(name, gui) {
	var folder = gui.__folders[name];
	if (!folder) {
		return;
	}
	folder.close();
	gui.__ul.removeChild(folder.domElement.parentNode);
	delete gui.__folders[name];
	gui.onResize();
}

function criarFolderGavetas() {
	var criarGaveta = {
		Nova_Gaveta: function () {
			let novaTextura = tipoTextura(textGavFinal);
			var gaveta = new Item(1, alturaGav, larguraGav, profundidadeGav, novaTextura);

			let armario = itemPai;
			let i;
			for (i = 0; i < arrayNomes.length; i++) {
				if (arrayNomes[i] == armGavFinal) {
					armario = arrayArmarios[i];
				}
			}
			if (armario.adicionarItemFilho(gaveta)) {
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				//listaTodosItens = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			} else {
				alert("Gaveta não cabe no armário - tente novamente");
			}
			listaTodosItens.push(gaveta);
			adicionarItemPai();
		}

	}
	//folder das gavetas
	var opcoesGavetas = gui.addFolder(nomeFolder2);
	parametersGavetas = {
		e: larguraGav, f: alturaGav, g: profundidadeGav
	}
	var textGav = opcoesGavetas.add(textInicialGav, 'Textura').options(listaTexturas);
	textGav.onChange(function () {
		textGavFinal = textInicialGav.Textura;
		if (meshClicada != null) { // se o item estiver selecionado
			if (itemClicado.tipo == 1) { // se for uma gaveta
				itemClicado.material = tipoTextura(textGavFinal);
				meshClicada.material.map = new THREE.TextureLoader().load(tipoTextura(textGavFinal));
			}
		}
	});
	var armGav = opcoesGavetas.add(armIni, 'Armario').options(arrayNomes);
	armGav.onChange(function () {
		armGavFinal = armIni.Armario;
	});
	var larguraGavDef = opcoesGavetas.add(parametersGavetas, 'e').min(60).max(300).listen().step(speed).name('Largura');
	var alturaGavDef = opcoesGavetas.add(parametersGavetas, 'f').min(90).max(220).listen().step(15).name('Altura');
	var profundidadeGavDef = opcoesGavetas.add(parametersGavetas, 'g').min(60).max(200).listen().step(speed).name('Profundidade');
	larguraGavDef.onChange(function (jar) {

		larguraGav = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 1) {
				let v = itemSelecionado.redefinirLargura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	alturaGavDef.onChange(function (jar) {

		alturaGav = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 1) {
				let v = itemSelecionado.redefinirAltura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	profundidadeGavDef.onChange(function (jar) {

		profundidadeGav = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 1) {
				let v = itemSelecionado.redefinirProfundidade(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	opcoesGavetas.add(criarGaveta, 'Nova_Gaveta').name('Criar gaveta');
}

function criarFolderPrateleiras() {
	var criarPrateleira = {
		Nova_Prateleira: function () {
			let novaTextura = tipoTextura(textPratFinal);
			var prateleira = new Item(2, alturaPrat, larguraPrat, profundidadePrat, novaTextura);

			let armario = itemPai;
			let i;
			for (i = 0; i < arrayNomes.length; i++) {
				if (arrayNomes[i] == armPratFinal) {
					armario = arrayArmarios[i];
				}
			}
			if (armario.adicionarItemFilho(prateleira)) {
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				//listaTodosItens = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			} else {
				alert("Prateleira não cabe no armário - tente novamente");
			}
			listaTodosItens.push(prateleira);
			adicionarItemPai();
		}
	}
	let opcoesPrateleiras = gui.addFolder(nomeFolder3);
	parametersPrateleiras = {
		e: larguraPrat, f: alturaPrat, g: profundidadePrat
	}
	var textPrat = opcoesPrateleiras.add(textInicialPrat, 'Textura').options(listaTexturas);
	textPrat.onChange(function () {
		textPratFinal = textInicialPrat.Textura;
		if (meshClicada != null) { // se o item estiver selecionado
			if (itemClicado.tipo == 2) { // se for uma prateleira
				itemClicado.material = tipoTextura(textPratFinal);
				meshClicada.material.map = new THREE.TextureLoader().load(tipoTextura(textPratFinal));
			}
		}
	});
	var armPrat = opcoesPrateleiras.add(armIni, 'Armario').options(arrayNomes);
	armPrat.onChange(function () {
		armPratFinal = armIni.Armario;
	});

	let larguraPratDef = opcoesPrateleiras.add(parametersPrateleiras, 'e').min(60).max(300).listen().step(speed).name('Largura');
	let alturaPratDef = opcoesPrateleiras.add(parametersPrateleiras, 'f').min(125).max(220).listen().step(25).name('Altura');
	let profundidadePratDef = opcoesPrateleiras.add(parametersPrateleiras, 'g').min(60).max(200).listen().step(speed).name('Profundidade');
	larguraPratDef.onChange(function (jar) {

		larguraPrat = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 2) {
				let v = itemSelecionado.redefinirLargura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	alturaPratDef.onChange(function (jar) {

		alturaPrat = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 2) {
				let v = itemSelecionado.redefinirAltura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	profundidadePratDef.onChange(function (jar) {

		profundidadePrat = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 2) {
				let v = itemSelecionado.redefinirProfundidade(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	opcoesPrateleiras.add(criarPrateleira, 'Nova_Prateleira').name('Criar prateleira');
}

function criarFolderSubArmario(local) {
	let folder = local.addFolder(nomeFolder4);
	var armArm = folder.add(armIni, 'Armario').options(arrayNomes);
	armArm.onChange(function () {
		armFinal = armIni.Armario;
	});
}

function criarFolderArmario() {
	var criarArmario = {
		Novo_Armario: function () {
			if (armCriado == false) {
				//controlsPerspective.maxPolarAngle = (Math.PI / ) - ((Math.PI / 2)/90);    // para não deixar ver abaixo do nível do solo
				armCriado = true;
				let novaTextura = tipoTextura(textArmFinal);
				itemPai.material = novaTextura;
				itemPai.redefinirAltura(altura);
				itemPai.largura = largura;
				itemPai.profundidade = profundidade;
				var armArm = opcoesArmario.add(armIni, 'Armario').options(arrayNomes);
				armArm.onChange(function () {
					armFinal = armIni.Armario;
				});
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);

				criarFolderGavetas();
				criarFolderPrateleiras();

				//criarFolderSubArmario(opcoesArmario);

			} else {
				let novaTextura = tipoTextura(textArmFinal);
				var novoArmario = new Item(0, altura, largura, profundidade, novaTextura);

				let armario = itemPai;
				let i;
				for (i = 0; i < arrayNomes.length; i++) {
					if (arrayNomes[i] == armFinal) {
						armario = arrayArmarios[i];
					}
				}
				if (armario.adicionarItemFilho(novoArmario)) {
					limparScene(scene, itemPai);
					limparScene(scene, itemPai);
					listaTodosMeshes = [];
					//listaTodosItens = [];
					desenharItem(itemPai, scene);
					abrirPortas(itemPai, 0, true);
					arrayArmarios.push(novoArmario);
					let nome = 'Armario ' + numArm;
					numArm++;
					arrayNomes.push(nome);
					removeFolder(nomeFolder2, gui);
					removeFolder(nomeFolder3, gui);
					removeFolder(nomeFolder1, gui);
					//removeFoldersFolder(nomeFolder1, nomeFolder4, gui);
					criarFolderArmario();
					criarFolderGavetas();
					criarFolderPrateleiras();
					//criarFolderSubArmario(opcoesArmario);
					//opcoesGavetas.remove(armGav);
					//armGav = opcoesGavetas.add(armIni, 'Armario').options(arrayNomes);
					//armGav.onChange(function () {
					//	armFinal = armIni.Armario;
					//});
					listaTodosItens.push(novoArmario);
				} else {
					alert("Armário não cabe no armário - tente novamente");
				}

			}

			adicionarItemPai();
		}
	};


	parameters = {
		e: largura, f: altura, g: profundidade
	}

	var opcoesArmario = gui.addFolder(nomeFolder1);

	var textInicialArm = { Textura: Textura1 };

	var textArm = opcoesArmario.add(textInicialArm, 'Textura').options(listaTexturas);
	var textArmFinal = textInicialArm;
	textArm.onChange(function () {
		textArmFinal = textInicialArm.Textura;
		if (meshClicada != null) { // se o item estiver selecionado
			if (itemClicado.tipo == 0) { // se for um armário
				itemClicado.material = tipoTextura(textArmFinal);
				meshClicada.material.map = new THREE.TextureLoader().load(tipoTextura(textArmFinal));
			}
		}
	});

	var larguraDef = opcoesArmario.add(parameters, 'e').min(60).max(300).listen().step(speed).name('Largura');
	var alturaDef = opcoesArmario.add(parameters, 'f').min(60).max(220).listen().step(speed).name('Altura');
	var profundidadeDef = opcoesArmario.add(parameters, 'g').min(60).max(200).listen().step(speed).name('Profundidade');
	larguraDef.onChange(function (jar) {

		largura = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 0) {
				let v = itemSelecionado.redefinirLargura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}

	});
	alturaDef.onChange(function (jar) {

		altura = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 0) {
				let v = itemSelecionado.redefinirAltura(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}
	});
	profundidadeDef.onChange(function (jar) {

		profundidade = jar;
		if (armCriado && meshClicada != null) {
			if (itemSelecionado.tipo == 0) {
				let v = itemSelecionado.redefinirProfundidade(jar);
				limparScene(scene, itemPai);
				listaTodosMeshes = [];
				desenharItem(itemPai, scene);
				abrirPortas(itemPai, 0, true);
			}
		}
	});



	opcoesArmario.add(criarArmario, 'Novo_Armario').name('Criar armário');
	if (armCriado == true) {
		var armArm = opcoesArmario.add(armIni, 'Armario').options(arrayNomes);
		armArm.onChange(function () {
			armFinal = armIni.Armario;
		});
	}
}

function adicionarItemPai() {
	let i;
	let existe = false;
	for (i = 0; i < listaTodosItens.length; i++) {
		if (listaTodosItens[i] == itemPai) {
			existe = true; break;
		}
	}
	if (!existe) listaTodosItens.push(itemPai);
}

function atualizarUI(itemClicado) {
	let tipoItem = itemClicado.tipo;
	itemSelecionado = itemClicado;
	switch (tipoItem) {
		case 0: // armário
			parameters.e = itemClicado.largura;
			parameters.f = itemClicado.altura;
			parameters.g = itemClicado.profundidade;
			break;
		case 1: // gaveta
			parametersGavetas.e = itemClicado.largura;
			parametersGavetas.f = itemClicado.altura;
			if (itemClicado.profundidade == 8) {
				parametersGavetas.g = itemClicado.profundidade + 2;
			} else {
				parametersGavetas.g = itemClicado.profundidade;
			}

			break;
		case 2: // prateleira
			parametersPrateleiras.e = itemClicado.largura;
			parametersPrateleiras.f = itemClicado.altura;
			parametersPrateleiras.g = itemClicado.profundidade;
			break;
		default:
			break;
	}
}

function atualizarFolders(novaListaItens) {
	let listaArmarios = [];
	let listaNomes = [];
	let i, j;
	for (i = 0; i < novaListaItens.length; i++) {
		if (novaListaItens[i].tipo == 0) {
			listaArmarios.push(novaListaItens[i]);
			j = i + 1;
			listaNomes.push("Armario " + j);
		}
	}

	if (listaArmarios.length > 0) {
		arrayArmarios = listaArmarios;
		arrayNomes = listaNomes;
		removeFolder(nomeFolder2, gui);
		removeFolder(nomeFolder3, gui);
		removeFolder(nomeFolder1, gui);
		criarFolderArmario();
		criarFolderGavetas();
		criarFolderPrateleiras();
	} else {
		removeFolder(nomeFolder2, gui);
		removeFolder(nomeFolder3, gui);
		removeFolder(nomeFolder1, gui);
		//removeFoldersFolder(nomeFolder1, nomeFolder4, gui);
		armCriado = false;
		criarFolderArmario();
	}
}
